const express = require("express");
const path = require("path");
const cors = require("cors");
const connectDB = require("./config/db");

const app = express();
app.use(express.json({ extended: false }));

const MODE = "PROD";

if (MODE === "PROD") {
  app.use(cors());
  app.use(express.static(path.join(__dirname, "client/build")));
}
if (MODE == "DEV") {
  app.use(
    cors({
      origin: "http://localhost:3000",
      methods: ["POST", "PUT", "GET", "OPTIONS", "HEAD"],
      credentials: true,
    })
  );
}

app.use("/", require("./routes"));
app.get("/*", (req, res) => {
  return res.sendFile(path.join(__dirname, "client/build/index.html"));
});

connectDB();
const PORT = process.env.PORT || 5000;
app.listen(PORT, () => console.log(`Running on port ${PORT}`));

const express = require("express");
const router = express.Router();
const ingredients = require("./ingredients");

router.post("/ingredients", ingredients.all);
router.post("/ingredients/:name/identity/update", ingredients.update_identity);
router.post("/ingredients/:name/feature/update", ingredients.update_feature);
router.post("/ingredients/:name/action/update", ingredients.update_action);

router.post("/ingredients/:name/identity", ingredients.identity);
router.post("/ingredients/:name/feature", ingredients.feature);
router.post("/ingredients/:name/action", ingredients.action);

module.exports = router;

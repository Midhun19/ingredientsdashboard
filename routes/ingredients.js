const Ingredients = require("../models/Ingredients");
const Actions = require("../models/Actions");

const exp = {};

exp.all = async (req, res) => {
  try {
    const ingredients = await Ingredients.find(
      {},
      { "identity.name": 1, "identity.imageURL": 1 }
    );
    res.send({ ingredients });
  } catch (err) {
    console.log(err.toString());
  }
};

exp.identity = async (req, res) => {
  try {
    const { name } = req.params;
    const ingredient = await Ingredients.findOne(
      { "identity.name": name },
      { identity: 1 }
    );
    res.send(ingredient);
  } catch (err) {
    console.log(err.toString());
  }
};

exp.feature = async (req, res) => {
  try {
    const { name } = req.params;
    const ingredient = await Ingredients.findOne(
      { "identity.name": name },
      { features: 1 }
    );
    res.send(ingredient);
  } catch (err) {
    console.log(err.toString());
  }
};

exp.action = async (req, res) => {
  try {
    const { name } = req.params;
    const actions = await Actions.find({}, { name: 1 });
    const obj = {};
    actions.map((action) => (obj[action.name] = false));
    const ingredient = await Ingredients.findOne(
      { "identity.name": name },
      { "actions.name": 1 }
    );
    ingredient.actions.map((action) => (obj[action.name] = true));
    return res.send(obj);
  } catch (err) {
    console.log(err.toString());
  }
};

exp.update_identity = async (req, res) => {
  try {
    const data = req.body;
    const { name } = req.params;
    const result = await Ingredients.updateOne(
      { "identity.name": name },
      { $set: { identity: data } }
    );
    return res.send(result);
  } catch (err) {
    console.log(err.toString());
  }
};

exp.update_feature = async (req, res) => {
  try {
    const data = req.body;
    const { name } = req.params;
    const result = await Ingredients.updateOne(
      { "identity.name": name },
      { $set: { features: data } }
    );
    return res.send(result);
  } catch (err) {
    console.log(err.toString());
  }
};

exp.update_action = async (req, res) => {
  try {
    const data = req.body;
    const { name } = req.params;
    const actions = [];

    Object.entries(data).map(([key, value]) => {
      if (value === true) {
        actions.push(key);
      }
    });

    let action_objects = await Actions.find({
      name: { $in: actions },
    }).select("-_id");

    await Ingredients.updateOne(
      { "identity.name": name },
      { $set: { actions: action_objects } }
    );

    return res.send(data);
  } catch (err) {
    console.log(err.toString());
  }
};

module.exports = exp;

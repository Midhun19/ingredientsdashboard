const mongoose = require("mongoose");
const url = "mongodb://localhost:27017/foodl";

const connectDB = async () => {
  try {
    await mongoose.connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    console.log("Database connected.");
  } catch (err) {
    console.log(err.toString());
  }
};

module.exports = connectDB;

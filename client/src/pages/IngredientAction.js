import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/Button";
import AlertSnackbar from "../components/Snackbar";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Checkbox from "@material-ui/core/Checkbox";

const useStyles = makeStyles((theme) => ({
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const IngredientAction = (props) => {
  const [action, setAction] = useState({});
  const { name } = useParams();
  const classes = useStyles();
  const [update, setUpdate] = useState(0);
  const [alert, setAlert] = useState(false);
  useEffect(() => {
    const getAction = async () => {
      const result = await axios.post(`/ingredients/${name}/action`, null, {
        headers: {
          "Content-Type": "application/json",
        },
      });

      setAction(result.data);
      setUpdate(1);
    };
    getAction();
  }, [name]);

  function handleChange(e) {
    const target = e.target;

    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    setAction({
      ...action,
      [name]: value,
    });
  }

  function handleSubmit() {
    const submit = async () => {
      return axios
        .post(`/ingredients/${name}/action/update`, action, {
          headers: {
            "Content-Type": "application/json",
          },
        })
        .then((result) => {
          setAlert(true);
        })
        .catch((error) => console.log(error));
    };
    submit();
  }

  return (
    <main className={classes.layout}>
      <AlertSnackbar alert={alert} setAlert={setAlert}></AlertSnackbar>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h4" align="center">
          {name}
        </Typography>
        <React.Fragment>
          <Typography variant="h6" gutterBottom>
            Ingredient Actions
          </Typography>
          {update ? (
            <>
              <List dense className={classes.root}>
                {Object.entries(action).map(([key, value]) => {
                  const labelId = `checkbox-list-secondary-label-${key}`;
                  return (
                    <ListItem key={key} button>
                      <ListItemText id={labelId} primary={`${key}`} />
                      <ListItemSecondaryAction>
                        <Checkbox
                          edge="end"
                          onChange={handleChange}
                          checked={value}
                          name={key}
                          type="checkbox"
                          inputProps={{ "aria-labelledby": labelId }}
                        />
                      </ListItemSecondaryAction>
                    </ListItem>
                  );
                })}
              </List>
            </>
          ) : (
            <></>
          )}
          <Grid container justify="flex-end">
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={handleSubmit}
                className={classes.button}
                startIcon={<SaveIcon />}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </React.Fragment>
      </Paper>
    </main>
  );
};

export default IngredientAction;

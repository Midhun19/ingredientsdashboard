import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/Button";
import AlertSnackbar from "../components/Snackbar";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const IngredientForm = (props) => {
  const [identity, setIdentity] = useState({});
  const { name } = useParams();
  const classes = useStyles();
  const [update, setUpdate] = useState(0);
  const [alert, setAlert] = useState(false);
  useEffect(() => {
    const getIngredient = async () => {
      const result = await axios.post(
        `/ingredients/${name}/identity`,
        null,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      setIdentity(result.data.identity);
      setUpdate(1);
    };
    getIngredient();
  }, [name]);

  function handleInputChange(e) {
    const target = e.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    setIdentity({
      ...identity,
      [name]: value,
    });
  }

  function handleSubmit() {
    const submit = async () => {
      return axios
        .post(
          `/ingredients/${name}/identity/update`,
          identity,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((result) => {
          setAlert(true);
        })
        .catch((error) => console.log(error));
    };
    submit();
  }

  return (
    <main className={classes.layout}>
      <AlertSnackbar alert={alert} setAlert={setAlert}></AlertSnackbar>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h4" align="center">
          {name}
        </Typography>
        <React.Fragment>
          <Typography variant="h6" gutterBottom>
            Ingredient Details
          </Typography>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="ingredientID"
                name="ingredientID"
                label="ingredientID"
                value={update ? identity.ingredientID : ``}
                onChange={handleInputChange}
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                required
                id="name"
                name="name"
                label="Name"
                value={update ? identity.name : ``}
                fullWidth
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                required
                id="state"
                name="state"
                label="State"
                value={update ? identity.state : ``}
                fullWidth
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="category"
                name="category"
                label="Category"
                fullWidth
                value={update ? identity.category : ``}
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12}>
              <FormControlLabel
                control={
                  <Checkbox
                    color="secondary"
                    name="atomic"
                    type="checkbox"
                    onChange={handleInputChange}
                    checked={update ? identity.atomic : false}
                  />
                }
                label="Atomic"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="imageurl"
                name="imageURL"
                label="Image URL"
                fullWidth
                value={update ? identity.imageURL : ``}
                onChange={handleInputChange}
              />
            </Grid>
          </Grid>
          <Grid container justify="flex-end">
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={handleSubmit}
                className={classes.button}
                startIcon={<SaveIcon />}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </React.Fragment>
      </Paper>
    </main>
  );
};

export default IngredientForm;

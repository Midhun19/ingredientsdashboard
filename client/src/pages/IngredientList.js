import React, { useEffect, useState } from "react";
import axios from "axios";
import MediaCard from "../components/Card";
import Grid from "@material-ui/core/Grid";
import withWidth, { isWidthUp } from "@material-ui/core/withWidth";
import NavBar from "../components/NavBar";
import Box from "@material-ui/core/Box";

const IngredientList = (props) => {
  const [ingredients, setIngredients] = useState([]);
  useEffect(() => {
    const getIngredients = async () => {
      const result = await axios.post("/ingredients", null, {
        headers: {
          "Content-Type": "application/json",
        },
        withCredentials: true,
      });
      setIngredients([...result.data.ingredients]);
    };
    getIngredients();
  }, [ingredients]);

  function getCols(screenWidth) {
    if (isWidthUp("lg", screenWidth)) {
      return 3;
    }

    if (isWidthUp("md", screenWidth)) {
      return 4;
    }

    if (isWidthUp("sm", screenWidth)) {
      return 6;
    }

    return 12;
  }

  const cols = getCols(props.width);

  return (
    <>
      <NavBar />
      <Box m={10} />
      <Grid container spacing={3} justify="center">
        {ingredients.map((ingredient, i) => {
          return (
            <Grid item xs={cols} align="center" key={i}>
              {ingredient.identity ? (
                <MediaCard
                  imageURL={ingredient.identity.imageURL}
                  name={ingredient.identity.name}
                ></MediaCard>
              ) : (
                <></>
              )}
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

export default withWidth()(IngredientList);

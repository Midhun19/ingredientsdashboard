import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/Button";
import AlertSnackbar from "../components/Snackbar";
import SwitchButton from "../components/SwitchButton";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  layout: {
    width: "auto",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: "auto",
      marginRight: "auto",
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  buttons: {
    display: "flex",
    justifyContent: "flex-end",
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));

const IngredientFeature = (props) => {
  const [feature, setFeature] = useState({});
  const { name } = useParams();
  const classes = useStyles();
  const [update, setUpdate] = useState(0);
  const [alert, setAlert] = useState(false);
  useEffect(() => {
    const getFeature = async () => {
      const result = await axios.post(
        `/ingredients/${name}/feature`,
        null,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      setFeature(result.data.features);
      setUpdate(1);
    };
    getFeature();
  }, [name]);

  function handleChange(e) {
    const target = e.target;

    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;
    setFeature({
      ...feature,
      [name]: value,
    });
  }

  function handleSubmit() {
    const submit = async () => {
      return axios
        .post(
          `/ingredients/${name}/feature/update`,
          feature,
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then((result) => {
          setAlert(true);
        })
        .catch((error) => console.log(error));
    };
    submit();
  }

  return (
    <main className={classes.layout}>
      <AlertSnackbar alert={alert} setAlert={setAlert}></AlertSnackbar>
      <Paper className={classes.paper}>
        <Typography component="h1" variant="h4" align="center">
          {name}
        </Typography>
        <React.Fragment>
          <Typography variant="h6" gutterBottom>
            Ingredient Features
          </Typography>
          <Grid item xs={6}>
            <TextField
              id="unit"
              name="unit"
              label="Unit"
              value={update ? feature.unit : ``}
              fullWidth
              onChange={handleChange}
            />
          </Grid>
          <SwitchButton
            check={update ? feature.quantifiable : false}
            handleChange={handleChange}
            name={"quantifiable"}
          />
          <SwitchButton
            check={update ? feature.timeSensitive : false}
            handleChange={handleChange}
            name={"timeSensitive"}
          />
          <SwitchButton
            check={update ? feature.toColorSensitive : false}
            handleChange={handleChange}
            name={"toColorSensitive"}
          />
          <SwitchButton
            check={update ? feature.canMash : false}
            handleChange={handleChange}
            name={"canMash"}
          />
          <SwitchButton
            check={update ? feature.canFineChop : false}
            handleChange={handleChange}
            name={"canFineChop"}
          />
          <SwitchButton
            check={update ? feature.canBlend : false}
            handleChange={handleChange}
            name={"canBlend"}
          />
          <SwitchButton
            check={update ? feature.canFry : false}
            handleChange={handleChange}
            name={"canFry"}
          />
          <SwitchButton
            check={update ? feature.canHeat : false}
            handleChange={handleChange}
            name={"canHeat"}
          />
          <SwitchButton
            check={update ? feature.canChill : false}
            handleChange={handleChange}
            name={"canChill"}
          />
          <SwitchButton
            check={update ? feature.canFreeze : false}
            handleChange={handleChange}
            name={"canFreeze"}
          />
          <Grid container justify="flex-end">
            <Grid item>
              <Button
                variant="contained"
                color="primary"
                size="large"
                onClick={handleSubmit}
                className={classes.button}
                startIcon={<SaveIcon />}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </React.Fragment>
      </Paper>
    </main>
  );
};

export default IngredientFeature;

import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import "./App.css";
import IngredientList from "./pages/IngredientList";
import TabPanel from "./pages/Dashboard";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" children={<IngredientList />} />
        <Route path="/ingredients/:name" children={<TabPanel />} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;

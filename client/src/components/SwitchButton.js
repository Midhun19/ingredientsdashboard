import React from "react";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

export default function SwitchButton({ check, handleChange, name }) {
  return (
    <FormGroup row>
      <FormControlLabel
        control={<Switch checked={check} onChange={handleChange} name={name} />}
        label={name}
        type="checkbox"
      />
    </FormGroup>
  );
}

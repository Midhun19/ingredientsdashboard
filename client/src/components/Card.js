import React from "react";
import { Link } from "react-router-dom";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

export default function MediaCard(props) {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={props.imageURL}
          title="Contemplative Reptile"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {props.name}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            A food ingredient is any substance that is added to a food to
            achieve a desired effect. The term “food ingredient” includes food
            additives, storage or packaging.
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Link
          style={{ textDecoration: "none" }}
          to={`/ingredients/${props.name}`}
        >
          <Button size="small" variant="contained" color="primary">
            UPDATE
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
}

const mongoose = require("mongoose");

const actionSchema = mongoose.Schema({
  actionID: {
    type: String,
    required: true,
  },

  name: {
    type: String,
    required: true,
  },

  description: String,

  precedence: {
    type: Number,
    required: true,
  },

  required: {
    type: Boolean,
    required: true,
  },

  time: Boolean,

  options: [String],

  repeatable: Boolean,

  notes: String,
});

module.exports = Actions = mongoose.model("action", actionSchema);

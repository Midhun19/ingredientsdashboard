const mongoose = require("mongoose");
const Actions = require("./Actions");

const ingredientSchema = new mongoose.Schema({
  identity: {
    name: {
      type: String,
      required: [true, "Enter a valid name"],
    },
    ingredientID: {
      type: String,
      required: [true, "Enter valid ingredientID"],
    },
    state: {
      type: String,
      required: true,
    },
    atomic: {
      type: Boolean,
    },
    category: String,

    imageURL: {
      type: String,
      required: true,
    },
    vend: {
      layer: {
        type: String,
      },
    },
  },

  features: {
    quantifiable: {
      type: Boolean,
      required: true,
    },

    unit: String,

    timeSensitive: {
      type: Boolean,
      required: true,
    },

    toColorSensitive: {
      type: Boolean,
      required: true,
    },

    canMash: {
      type: Boolean,
      required: true,
    },

    canCrush: {
      type: Boolean,
      required: true,
    },

    canFineChop: {
      type: Boolean,
      required: true,
    },

    canBlend: {
      type: Boolean,
      required: true,
    },

    canFry: {
      type: Boolean,
      required: true,
    },

    canHeat: {
      type: Boolean,
      required: true,
    },

    canChill: {
      type: Boolean,
      required: true,
    },

    canFreeze: {
      type: Boolean,
      required: true,
    },
  },

  actions: {
    type: [Actions.schema],
    required: true,
  },
});

module.exports = Ingredients = mongoose.model("ingredient", ingredientSchema);
